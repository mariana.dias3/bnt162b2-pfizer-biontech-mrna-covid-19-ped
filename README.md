# BNT162b2-Pfizer-BioNTech-mRNA-COVID-19-Children


## Description
This file pertains to the code used for conducting the analysis of the manuscript , related to the study registered under the following number NCT05403307, developed in RStudio version 4.3.1.

## Authors 
This code was jointly developed by Mariana Motta and Daniel Sganzerla, based on the previous activities by Fernanda Kelly Romeiro.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
